﻿namespace Application374Rule
{
    using Common.Enums;
    using Common.Interfaces;
    using System.Collections.Generic;

    public class Application374Rule : IApplicationRule
    {
        public Application374Rule()
        {
            ComputerIds = new HashSet<int>();
        }

        /// <summary>
        /// Gets the list of computer ids.
        /// </summary>
        /// <remarks>
        /// This assumes that the computer id is unique regardless of the type of computer.
        /// </remarks>
        private HashSet<int> ComputerIds { get; }

        /// <summary>
        /// Gets or sets the count of desktops not paired with laptops.
        /// </summary>
        private int CountOfDesktops { get; set; }

        /// <summary>
        /// Gets or sets the count of laptops not paired with desktops.
        /// </summary>
        private int CountOfLaptops { get; set; }

        public bool IsNewApplicationRequired(int computerId, ComputerTypes computerType)
        {
            // if this is a duplicate don't add a purchase requirement.
            if (IsDuplicate(computerId))
            {
                return false;
            }

            // if adding a laptop and no free desktop an app purchase is required
            if (computerType == ComputerTypes.Laptop && CountOfDesktops == 0)
            {
                CountOfLaptops++;
                return true;
            }

            // as there is a desktop without a corresponding laptop they can be paired, no additional purchase required
            if (computerType == ComputerTypes.Laptop)
            {
                CountOfDesktops--;
                return false;
            }

            // if anything other than a desktop then this rule isn't interested. Just in case someone adds a new type.
            if (computerType != ComputerTypes.Desktop)
            {
                return false;
            }

            // if there is a free laptop this can be paired against
            if (CountOfLaptops > 0)
            {
                CountOfLaptops--;
                return false;
            }

            // must be adding a desktop
            CountOfDesktops++;
            return true;
        }

        /// <summary>
        /// Keep a list of the ids of the computers this has been added to, to prevent adding a duplicate.
        /// </summary>
        /// <param name="computerId"></param>
        /// <returns>true if a record exists, false otherwise.</returns>
        private bool IsDuplicate(int computerId)
        {
            return !ComputerIds.Add(computerId);
        }
    }
}