﻿namespace Application374Rule
{
    using Common.Enums;
    using Common.Interfaces;
    using System.Collections.Generic;

    public class Installation374Rule : IInstallationRule
    {
        /// <summary>
        /// A list of the users and the rule state for that user.
        /// </summary>
        private readonly IDictionary<int, IApplicationRule> usersList;

        public Installation374Rule()
        {
            NumberOfCopiesRequired = 0;
            usersList = new Dictionary<int, IApplicationRule>();
        }

        public long NumberOfCopiesRequired { get; private set; }

        public void AddInstallation(int computerId, int userId, ComputerTypes computerTypes)
        {
            IApplicationRule rule;
            if (usersList.ContainsKey(userId))
            {
                rule = usersList[userId];
            }
            else
            {
                rule = new Application374Rule();
                usersList[userId] = rule;
            }

            if (rule.IsNewApplicationRequired(computerId, computerTypes))
            {
                NumberOfCopiesRequired++;
            }
        }
    }
}