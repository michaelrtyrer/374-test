﻿namespace SimpleSystemTest
{
    using Application374Rule;
    using ApplyRulesToFiles;
    using Common.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class SimpleSystemTest
    {
        public void Test()
        {
            while (true)
            {
                var pathToFile = GetPathToFile();

                if (!File.Exists(pathToFile))
                {
                    return;
                }

                IDictionary<int, IInstallationRule> ruleDictionary = GetRulesForTest();

                ICountApplicationRequirements countApplicationRequirements = new CountApplicationRequirements();
                countApplicationRequirements.ApplyRulesForFile(pathToFile, ruleDictionary);

                foreach (var rule in ruleDictionary)
                {
                    Console.WriteLine($"Rule {rule.Key} - Copies required = {rule.Value.NumberOfCopiesRequired}");
                }
            }
        }

        private string GetPathToFile()
        {
            Console.Write("Enter full path to file or return to finish> ");
            return Console.ReadLine();
        }

        private IDictionary<int, IInstallationRule> GetRulesForTest()
        {
            IInstallationRule applicationRule = new Installation374Rule();
            const int applicationId = 374;

            return new Dictionary<int, IInstallationRule>
            {
                { applicationId, applicationRule }
                // add other rules here ...
            };
        }
    }
}