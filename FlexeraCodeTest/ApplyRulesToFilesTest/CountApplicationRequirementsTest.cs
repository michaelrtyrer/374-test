﻿namespace ApplyRulesToFilesTest
{
    using ApplyRulesToFiles;
    using Common.Enums;
    using Common.Interfaces;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System.Collections.Generic;

    [TestClass]
    public class CountApplicationRequirementsTest
    {
        private MockRepository repository;
        private Mock<IInstallationRule> mockInstallationRule;

        [TestInitialize]
        public void TestStartup()
        {
            repository = new MockRepository(MockBehavior.Loose) { DefaultValue = DefaultValue.Mock };
            mockInstallationRule = repository.Create<IInstallationRule>();
        }

        [TestMethod]
        public void SingleItem_ItemAddedTest()
        {
            // Assign
            const int applicationId = 374;
            const string filePath = @"TestFiles\OneDesktop.csv";

            ICountApplicationRequirements itemUnderTest = new CountApplicationRequirements();
            var rules = GetRulesForTest(applicationId);

            // Act
            itemUnderTest.ApplyRulesForFile(filePath, rules);

            // Assert
            mockInstallationRule.Verify(x => x.AddInstallation(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<ComputerTypes>()), Times.Once, "It was expected to add once");
        }

        [TestMethod]
        public void SingleItem_ItemNotAddedTest()
        {
            // Assign
            const int applicationId = 999;
            const string filePath = @"TestFiles\OneDesktop.csv";

            ICountApplicationRequirements itemUnderTest = new CountApplicationRequirements();
            var rules = GetRulesForTest(applicationId);

            // Act
            itemUnderTest.ApplyRulesForFile(filePath, rules);

            // Assert
            mockInstallationRule.Verify(x => x.AddInstallation(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<ComputerTypes>()), Times.Never, "It was expected Not to add item");
        }

        private IDictionary<int, IInstallationRule> GetRulesForTest(int applicationId)
        {
            IInstallationRule applicationRule = mockInstallationRule.Object;

            return new Dictionary<int, IInstallationRule>
            {
                { applicationId, applicationRule }
                // add other rules here ...
            };
        }
    }
}