﻿namespace Common.Interfaces
{
    using Common.Enums;

    public interface IApplicationRule
    {
        /// <summary>
        /// Decide if a new application is required for the computerId and type combination.
        /// </summary>
        /// <param name="computerId">The Id of the computer where the application is installed.</param>
        /// <param name="computerType">The computer type to be added.</param>
        /// <returns>true if this increases the count of applications required, false if it pairs up with a spare.</returns>
        bool IsNewApplicationRequired(int computerId, ComputerTypes computerType);
    }
}
