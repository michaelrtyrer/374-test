﻿namespace Common.Interfaces
{
    using System.Collections.Generic;

    public interface ICountApplicationRequirements
    {
        /// <summary>
        /// Take a file path to a CSV file and apply the application rules to count requirements.
        /// </summary>
        /// <param name="filePath">Full path to CSV file</param>
        /// <param name="ruleDictionary">The list of rules that are to be applied on the data in the file.</param>
        void ApplyRulesForFile(string filePath, IDictionary<int, IInstallationRule> ruleDictionary);
    }
}
