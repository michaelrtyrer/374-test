﻿namespace Common.Interfaces
{
    using Common.Enums;

    /// <summary>
    /// A rule definition.
    /// </summary>
    public interface IInstallationRule
    {
        /// <summary>
        /// The number of copies of the application required.
        /// </summary>
        long NumberOfCopiesRequired { get; }

        /// <summary>
        /// Add a new installation to the collection.
        /// </summary>
        /// <param name="computerId">The Id of the collection being added.</param>
        /// <param name="userId">The Id of the user.</param>
        /// <param name="computerTypes">The computer type.</param>
        void AddInstallation(int computerId, int userId, ComputerTypes computerTypes);
    }
}