﻿namespace Common.DataTransfer
{
    using CsvHelper.Configuration.Attributes;

    /// <summary>
    /// A data transfer type for the records expected to be in the CSV files.
    /// </summary>
    public class UserRecord
    {
        /// <summary>
        /// Gets or sets the id of the application.
        /// </summary>
        [Name("ApplicationID")]
        public int ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the comment associated with the entry.
        /// </summary>
        [Name("Comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the id of the computer the application is stored on.
        /// </summary>
        [Name("ComputerID")]
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the type of computer device the application is being installed on.
        /// </summary>
        [Name("ComputerType")]
        public string ComputerType { get; set; }

        /// <summary>
        /// Gets or sets the id of the user of the application.
        /// </summary>
        [Name("UserID")]
        public int UserId { get; set; }
    }
}