﻿namespace ApplyRulesToFiles
{
    using Common.DataTransfer;
    using Common.Enums;
    using Common.Interfaces;
    using CsvHelper;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;

    public class CountApplicationRequirements : ICountApplicationRequirements
    {
        public void ApplyRulesForFile(string filePath, IDictionary<int, IInstallationRule> ruleDictionary)
        {
            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.HasHeaderRecord = true;
                csv.Read();
                csv.ReadHeader();

                while (csv.Read())
                {
                    var record = csv.GetRecord<UserRecord>();

                    // validate record, if not valid skip to next
                    if (!IsValidRecord(record))
                    {
                        continue;
                    }

                    var applicationId = record.ApplicationId;

                    if (ruleDictionary.ContainsKey(applicationId))
                    {
                        var rule = ruleDictionary[applicationId];

                        var computerType = (ComputerTypes)Enum.Parse(typeof(ComputerTypes), record.ComputerType, true);
                        rule.AddInstallation(record.ComputerId, record.UserId, computerType);
                    }
                }
            }
        }

        private bool IsValidRecord(UserRecord record)
        {
            // Ids expected to be positive.
            if (record.ComputerId < 1 || record.UserId < 1 || record.ApplicationId < 1)
            {
                return false;
            }

            try
            {
                var computerType = Enum.Parse(typeof(ComputerTypes), record.ComputerType, true);
            }
            catch (ArgumentNullException)
            {
                return false;
            }
            catch (OverflowException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }
    }
}