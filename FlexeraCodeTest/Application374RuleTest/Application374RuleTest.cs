﻿namespace Application374RuleTest
{
    using Application374Rule;
    using Common.Enums;
    using Common.Interfaces;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class Application374RuleTest
    {
        [TestMethod]
        public void AppRuleAddDestopAndLaptopTest()
        {
            // Assign
            IApplicationRule itemUnderTest = new Application374Rule();

            // Act/assert
            var actual = itemUnderTest.IsNewApplicationRequired(1, ComputerTypes.Desktop);
            Assert.IsTrue(actual, "Expected to add a new desktop.");

            actual = itemUnderTest.IsNewApplicationRequired(2, ComputerTypes.Laptop);
            Assert.IsFalse(actual, "Not expected to add a new laptop as there should be a desktop it can associate with.");
        }

        [TestMethod]
        public void AppRuleAddDuplicateDesktopTest()
        {
            // Assign
            IApplicationRule itemUnderTest = new Application374Rule();

            // Act/assert
            var actual = itemUnderTest.IsNewApplicationRequired(2, ComputerTypes.Desktop);
            Assert.IsTrue(actual, "Expected to add a new desktop.");

            actual = itemUnderTest.IsNewApplicationRequired(2, ComputerTypes.Desktop);
            Assert.IsFalse(actual, "Not expected to add a new desktop as there should already be a desktop with the same id.");
        }

        [TestMethod]
        public void AppRuleAddLaptopThenDesktopTest()
        {
            // Assign
            IApplicationRule itemUnderTest = new Application374Rule();

            // Act/assert
            var actual = itemUnderTest.IsNewApplicationRequired(2, ComputerTypes.Laptop);
            Assert.IsTrue(actual, "Expected to add a new laptop.");

            actual = itemUnderTest.IsNewApplicationRequired(1, ComputerTypes.Desktop);
            Assert.IsFalse(actual, "Not expected to add a new desktop as there should be a laptop it can associate with.");
        }

        [TestMethod]
        public void AppRuleAddMultipleDesktopsTest()
        {
            // Assign
            IApplicationRule itemUnderTest = new Application374Rule();

            // Act
            var actual = itemUnderTest.IsNewApplicationRequired(1, ComputerTypes.Desktop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new desktop.");

            // Act
            actual = itemUnderTest.IsNewApplicationRequired(2, ComputerTypes.Desktop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new desktop.");

            // Act
            actual = itemUnderTest.IsNewApplicationRequired(3, ComputerTypes.Desktop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new desktop.");
        }

        [TestMethod]
        public void AppRuleAddMultipleLaptopsTest()
        {
            // Assign
            IApplicationRule itemUnderTest = new Application374Rule();

            // Act
            var actual = itemUnderTest.IsNewApplicationRequired(1, ComputerTypes.Laptop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new laptop.");

            // Act
            actual = itemUnderTest.IsNewApplicationRequired(2, ComputerTypes.Laptop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new laptop.");

            // Act
            actual = itemUnderTest.IsNewApplicationRequired(3, ComputerTypes.Laptop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new laptop.");
        }

        [TestMethod]
        public void AppRuleAddSingleDesktopTest()
        {
            // Assign
            IApplicationRule itemUnderTest = new Application374Rule();

            // Act
            var actual = itemUnderTest.IsNewApplicationRequired(1, ComputerTypes.Desktop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new desktop.");
        }

        [TestMethod]
        public void AppRuleAddSingleLaptopTest()
        {
            // Assign
            IApplicationRule itemUnderTest = new Application374Rule();

            // Act
            var actual = itemUnderTest.IsNewApplicationRequired(1, ComputerTypes.Laptop);

            // Assert
            Assert.IsTrue(actual, "Expected to add a new laptop.");
        }
    }
}