﻿namespace Application374RuleTest
{
    using Application374Rule;
    using Common.Enums;
    using Common.Interfaces;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class Installation374RuleTest
    {
        /// <summary>
        /// Test adding duplicates
        /// </summary>
        [TestMethod]
        public void AddDuplicateLaptopTest()
        {
            // Assign
            const int computer1Id = 1;
            const int user1Id = 1;

            IInstallationRule itemUnderTest = new Installation374Rule();

            // Act/Assert
            itemUnderTest.AddInstallation(computer1Id, user1Id, ComputerTypes.Laptop);
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 1, "Expected to add a new laptop.");

            itemUnderTest.AddInstallation(computer1Id, user1Id, ComputerTypes.Laptop);
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 1, "Expected to ignore duplicate.");
        }

        [TestMethod]
        public void AddLaptopFollowedByDesktopSameUserTest()
        {
            // Assign
            const int computer1Id = 1;
            const int user1Id = 1;
            const int computer2Id = 2;

            IInstallationRule itemUnderTest = new Installation374Rule();

            // Act/Assert
            itemUnderTest.AddInstallation(computer2Id, user1Id, ComputerTypes.Laptop);
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 1, "Expected to add a new laptop.");

            itemUnderTest.AddInstallation(computer1Id, user1Id, ComputerTypes.Desktop);
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 1, "Expected to pair laptop with desktop.");
        }

        /// <summary>
        /// Test order of adding not relevant
        /// </summary>
        [TestMethod]
        public void AddLaptopFollowedByDesktopDifferentUserTest()
        {
            // Assign
            const int computer1Id = 1;
            const int user1Id = 1;
            const int computer2Id = 2;
            const int user2Id = 2;

            IInstallationRule itemUnderTest = new Installation374Rule();

            // Act/Assert
            itemUnderTest.AddInstallation(computer2Id, user1Id, ComputerTypes.Laptop);
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 1, "Expected to add a new laptop.");

            itemUnderTest.AddInstallation(computer1Id, user2Id, ComputerTypes.Desktop);
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 2, "Expected to pair laptop with desktop.");
        }

        [TestMethod]
        public void AddSingleDesktopTest()
        {
            // Assign
            const int computer1Id = 1;
            const int user1Id = 1;

            IInstallationRule itemUnderTest = new Installation374Rule();

            // Act
            itemUnderTest.AddInstallation(computer1Id, user1Id, ComputerTypes.Desktop);

            // Assert
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 1, "Expected to add a new desktop.");
        }

        [TestMethod]
        public void AddSingleLaptopTest()
        {
            // Assign
            const int computer1Id = 1;
            const int user1Id = 1;

            IInstallationRule itemUnderTest = new Installation374Rule();

            // Act
            itemUnderTest.AddInstallation(computer1Id, user1Id, ComputerTypes.Laptop);

            // Assert
            Assert.IsTrue(itemUnderTest.NumberOfCopiesRequired == 1, "Expected to add a new laptop.");
        }
    }
}